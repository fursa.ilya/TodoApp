package com.todo.fursa

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.todo.fursa.data.InnerDB
import com.todo.fursa.data.entity.Note
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class RoomDatabaseTest {

    lateinit var context: Context

    @Before
    fun setupContext() {
        context = InstrumentationRegistry.getInstrumentation().context
    }

    private val db by lazy {
        InnerDB.getInnerDB(context, true)
    }

    private val dao by lazy {
        db.getNoteDao()
    }

    @Test
    fun add() {
        Flowable.just(Note("Lorem ipsum", "Lorem ipsum", 1, System.currentTimeMillis()))
                .observeOn(Schedulers.io())
                .subscribe { dao.insert(it) }
        dao.getAll()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { Assert.assertNotEquals(0, it.count())}
    }

    @After
    fun flush() {
        db.close()
    }

}
