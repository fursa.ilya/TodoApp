package com.todo.fursa.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Note")
data class Note(
        @ColumnInfo(name = "title")
        var title: String,
        @ColumnInfo(name = "content")
        var content: String?,
        @ColumnInfo(name = "created_time")
        var createdTime: Long
) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0

}
