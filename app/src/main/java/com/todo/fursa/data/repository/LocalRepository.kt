package com.todo.fursa.data.repository

import com.todo.fursa.AppDelegate
import com.todo.fursa.data.dao.NoteDao
import com.todo.fursa.data.entity.Note
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class LocalRepository {

    init {
        AppDelegate.appComponent.inject(this@LocalRepository)
    }

    @Inject
    lateinit var noteDao: NoteDao

    fun addNote(newNote: Note) {
        noteDao.add(newNote)
    }

    fun getNote(id: Long): Single<Note> {
        return noteDao.get(id)
    }

    fun getAllNotes(): Flowable<MutableList<Note>> {
        return noteDao.getAll()
    }
}
