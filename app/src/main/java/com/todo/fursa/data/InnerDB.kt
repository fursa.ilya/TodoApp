package com.todo.fursa.data

import android.content.Context
import androidx.room.*
import com.todo.fursa.data.converter.DateConverter
import com.todo.fursa.data.dao.NoteDao
import com.todo.fursa.data.entity.Note
import com.todo.fursa.util.Const.DB_NAME

@Database(entities = [Note::class],
        version = 7,
        exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class InnerDB: RoomDatabase() {

    companion object {
        private lateinit var LOCAL_DB_INSTANCE: InnerDB

        @Synchronized fun getInnerDB(c: Context, inMemory: Boolean): InnerDB {
            LOCAL_DB_INSTANCE = if(inMemory) {
                Room.inMemoryDatabaseBuilder(c.applicationContext, InnerDB::class.java)
                        .fallbackToDestructiveMigration()
                        .build()
            } else {
                Room.databaseBuilder(c.applicationContext, InnerDB::class.java, DB_NAME)
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
            }

            return LOCAL_DB_INSTANCE
        }
    }

    abstract fun getNoteDao(): NoteDao
}