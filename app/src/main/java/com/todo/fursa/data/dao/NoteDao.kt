package com.todo.fursa.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.todo.fursa.data.entity.Note
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(newNote: Note)

    @Query("SELECT * FROM Note WHERE Note.id = :id")
    fun get(id: Long): Single<Note>

    @Query("SELECT * FROM Note")
    fun getAll(): Flowable<MutableList<Note>>

}