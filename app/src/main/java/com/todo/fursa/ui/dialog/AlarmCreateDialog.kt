package com.todo.fursa.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.button.MaterialButton
import com.todo.fursa.R
import com.todo.fursa.ui.fragment.PlaceFragment
import com.todo.fursa.ui.fragment.TimeFragment

class AlarmCreateDialog : BottomSheetDialogFragment() {
    private lateinit var radioTime: RadioButton
    private lateinit var radioPlace: RadioButton
    private lateinit var btnAdd: MaterialButton
    private lateinit var btnCancel: MaterialButton

    companion object {
        fun newInstance(): AlarmCreateDialog {
            return AlarmCreateDialog()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.alarm_time_dialog, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navigateTo(TimeFragment.newInstance())

        radioTime = view.findViewById(R.id.radioButtonTime)
        radioPlace = view.findViewById(R.id.radioButtonPlace)
        btnAdd = view.findViewById(R.id.btnAdd)
        btnCancel = view.findViewById(R.id.btnCancel)

        radioTime.setOnClickListener {
            radioPlace.isChecked = false
            navigateTo(TimeFragment.newInstance()) }
        radioPlace.setOnClickListener {
            radioTime.isChecked = false
            navigateTo(PlaceFragment.newInstance())
        }

        btnAdd.setOnClickListener {  }
        btnCancel.setOnClickListener { dismiss() }
    }

    private fun navigateTo(fragment: Fragment) {
        this.childFragmentManager
                .beginTransaction()
                .replace(R.id.containerLayout, fragment)
                .commit()

    }
}