package com.todo.fursa.ui.base

import android.os.Bundle

import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity

import com.google.android.material.bottomappbar.BottomAppBar
import com.todo.fursa.ui.dialog.MainBottomSheetDialog

abstract class BaseActivity : AppCompatActivity() {

    @LayoutRes
    abstract fun setUpContentView(): Int

    abstract fun setActivityActionBar(): BottomAppBar

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(setUpContentView())
        setSupportActionBar(setActivityActionBar())
    }


    protected fun showMainDrawer() {
        val bottomSheetDialog = MainBottomSheetDialog.newInstance()
        bottomSheetDialog.show(supportFragmentManager, bottomSheetDialog.tag)
    }

}
