package com.todo.fursa.ui.activity

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.bottomappbar.BottomAppBar
import com.todo.fursa.AppDelegate
import com.todo.fursa.R
import com.todo.fursa.ui.base.BaseActivity
import com.todo.fursa.ui.fragment.NewNoteDialog
import com.todo.fursa.ui.recycler.NoteAdapter
import com.todo.fursa.ui.viewmodel.AppViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var viewModel: AppViewModel

    private val adapter = NoteAdapter()
    private val disposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppDelegate.appComponent.inject(this@MainActivity)
        viewModel = ViewModelProviders.of(this@MainActivity).get(AppViewModel::class.java)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
        recyclerView.addItemDecoration(DividerItemDecoration(this@MainActivity, DividerItemDecoration.VERTICAL))
        swipeRefreshLayout.setOnRefreshListener(this@MainActivity)
        loadNotes()
    }

    override fun onResume() {
        super.onResume()

        fab.setOnClickListener {
            val dialog = NewNoteDialog.newInstance()
            dialog.show(supportFragmentManager, dialog.tag)
        }
    }

    override fun onRefresh() {
       swipeRefreshLayout.post {
           loadNotes()
       }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) { showMainDrawer() }
        return true
    }


    override fun setUpContentView(): Int {
        return R.layout.activity_main
    }

    override fun setActivityActionBar(): BottomAppBar {
        return bottom_app_bar
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    private fun loadNotes() {
        disposable.add(
            viewModel.getAllNotes()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { swipeRefreshLayout.isRefreshing = true }
                    .doOnError {
                        emptyListLayout.visibility = View.VISIBLE
                        recyclerView.visibility = View.GONE
                    }
                    .subscribe { notes ->
                        run {
                            if (notes.isNotEmpty()) {
                                emptyListLayout.visibility = View.GONE
                                recyclerView.visibility = View.VISIBLE
                                adapter.setNotes(notes)
                                swipeRefreshLayout.isRefreshing = false
                            }
                        }
                    }
        )
    }

}
