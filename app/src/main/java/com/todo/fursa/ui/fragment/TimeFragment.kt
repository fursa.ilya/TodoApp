package com.todo.fursa.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.todo.fursa.R

class TimeFragment : Fragment() {

    companion object {
        fun newInstance(): TimeFragment {
            return TimeFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.time_layout, container, false)
    }
}