package com.todo.fursa.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.todo.fursa.AppDelegate
import com.todo.fursa.data.entity.Note
import com.todo.fursa.data.repository.LocalRepository
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class AppViewModel(application: Application) : AndroidViewModel(application) {

    init {
        AppDelegate.appComponent.inject(this@AppViewModel)
    }

    @Inject
    lateinit var repository: LocalRepository

    fun addNote(newNote: Note) {
        repository.addNote(newNote)
    }

    fun getNote(id: Long): Single<Note> {
        return repository.getNote(id)
    }

    fun getAllNotes(): Flowable<MutableList<Note>> {
        return repository.getAllNotes()
    }

}