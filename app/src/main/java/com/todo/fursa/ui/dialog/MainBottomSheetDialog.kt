package com.todo.fursa.ui.dialog

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.navigation.NavigationView
import com.todo.fursa.R
import com.todo.fursa.util.toast

const val RATE_APP_URL = "https://play.google.com/store/apps/details?id=com.vkontakte.android&hl=ru"

class MainBottomSheetDialog : BottomSheetDialogFragment() {
    private lateinit var navigationView: NavigationView

    companion object {
        fun newInstance(): MainBottomSheetDialog {
            return MainBottomSheetDialog()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.main_bottomsheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigationView = view.findViewById(R.id.mainNavView)
        navigationView.setNavigationItemSelectedListener { menuItem ->
            when(menuItem.itemId) {
                R.id.app_bar_profile -> { toast(view.context, "Profile") }
                R.id.app_bar_settings -> { openSettingsDialog() }
                R.id.app_bar_rate_us -> { rateUs() }
                R.id.app_bar_email_us -> { emailUs() }
            }
            true
        }
    }

    private fun rateUs() {
        val rateIntent = Intent(Intent.ACTION_VIEW)
        rateIntent.data = Uri.parse(RATE_APP_URL)
        startActivity(rateIntent)
    }

    private fun openSettingsDialog() {
      toast(dialog.context, "Settings")
    }

    private fun emailUs() {
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, "fursa.ilya@protonmail.com")
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Test email title")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Hello, world!")
        startActivity(Intent.createChooser(emailIntent, "Отправить письмо"))
    }

}