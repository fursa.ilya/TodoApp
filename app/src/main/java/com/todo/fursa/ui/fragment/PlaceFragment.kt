package com.todo.fursa.ui.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.todo.fursa.R
import com.todo.fursa.util.toast

class PlaceFragment: Fragment(){
    private var fields = listOf(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME, Place.Field.ADDRESS)
    private lateinit var placeTextView: TextView

    companion object {
        const val AUTOCOMPLETE_REQUEST_CODE = 1

        fun newInstance(): PlaceFragment {
            return PlaceFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.place_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        placeTextView = view.findViewById(R.id.placeTextView)
        placeTextView.setOnClickListener {
            pickLocation(view)
        }

    }

    private fun pickLocation(view: View) {
        val placePickingIntent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
        ).setTypeFilter(TypeFilter.ADDRESS)
                .build(view.context)
        startActivityForResult(placePickingIntent, AUTOCOMPLETE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data!!)
                placeTextView.text = "${place.address}"
            } else if(resultCode == AutocompleteActivity.RESULT_ERROR) {
                val status = Autocomplete.getStatusFromIntent(data!!)
                toast(context!!, "Status: ${status.statusMessage}")
            }
        }
    }

}