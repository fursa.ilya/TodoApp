package com.todo.fursa.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders

import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.button.MaterialButton
import com.todo.fursa.R
import com.todo.fursa.data.entity.Note
import com.todo.fursa.ui.recycler.NoteAdapter
import com.todo.fursa.ui.viewmodel.AppViewModel

class NewNoteDialog : BottomSheetDialogFragment() {
    private lateinit var etTitle: EditText
    private lateinit var etContent: EditText
    private lateinit var tvClock: TextView
    private lateinit var tvAddContent: TextView
    private lateinit var buttonSubmit: MaterialButton

    private lateinit var viewModel: AppViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.BottomSheetDialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.new_note_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        etTitle = view.findViewById(R.id.etTitle)
        etContent = view.findViewById(R.id.etContent)
        tvClock = view.findViewById(R.id.tvTime)
        tvAddContent = view.findViewById(R.id.tvDescription)
        buttonSubmit = view.findViewById(R.id.buttonSubmit)
        viewModel = ViewModelProviders.of(this@NewNoteDialog).get(AppViewModel::class.java)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tvAddContent.setOnClickListener {
           addDescriptionField()
        }

        buttonSubmit.setOnClickListener {
            addNote()
        }

    }

    companion object {

        fun newInstance(): NewNoteDialog {
            return NewNoteDialog()
        }
    }

    private fun addNote() {
        if(etContent.visibility == View.VISIBLE) {
            val content = etContent.text.toString()
            val title = etTitle.text.toString()
            viewModel.addNote(Note(title, content, System.currentTimeMillis()))
        } else {
            val title = etTitle.text.toString()
            viewModel.addNote(Note(title, null, System.currentTimeMillis()))
        }
    }

    private fun addDescriptionField() {
        if(etContent.visibility == View.VISIBLE) {
            etContent.visibility = View.GONE
        } else {
            etContent.visibility = View.VISIBLE
        }
    }
}
