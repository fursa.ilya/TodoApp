package com.todo.fursa.ui.recycler

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.todo.fursa.R
import com.todo.fursa.data.entity.Note

class NoteAdapter: RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {
    private val notes = mutableListOf<Note>()

    fun setNotes(notes: MutableList<Note>) {
        this.notes.clear()
        this.notes.addAll(notes)
        notifyDataSetChanged()
    }

    fun addNote(it: Note) {
        this.notes.add(it)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val noteView = LayoutInflater.from(parent.context).inflate(R.layout.task_row, parent, false)
        return NoteViewHolder(noteView)
    }

    override fun getItemCount(): Int {
        return notes.count()
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val currentNote = notes[position]
        holder.bind(currentNote)
    }

    inner class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val title = itemView.findViewById<TextView>(R.id.taskTitle)
        private val content = itemView.findViewById<TextView>(R.id.taskText)
        private val taskDone = itemView.findViewById<CheckBox>(R.id.taskDone)

        fun bind(currentNote: Note) {
            title.text = currentNote.title
            content.text = currentNote.content
        }

    }
}