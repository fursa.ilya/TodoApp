package com.todo.fursa.di

import android.app.Application
import com.todo.fursa.data.InnerDB
import com.todo.fursa.data.repository.LocalRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule(app: Application) {
    private val db by lazy { InnerDB.getInnerDB(app.applicationContext, false) }

    @Singleton
    @Provides
    fun provideNoteRepository() = LocalRepository()

    @Singleton
    @Provides
    fun provideNoteDao() = db.getNoteDao()

    @Singleton
    @Provides
    fun provideInnerDB() = db

}
