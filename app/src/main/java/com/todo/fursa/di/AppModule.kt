package com.todo.fursa.di

import android.app.Application
import android.app.NotificationManager
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(app: Application) {

    private val context by lazy {
        app.applicationContext
    }

    @Singleton
    @Provides
    fun provideAppContext(): Context = context

    @Singleton
    @Provides
    fun provideNotificationManager(): NotificationManager {
        return context
                .getSystemService(Context.NOTIFICATION_SERVICE)
                as NotificationManager
    }
}