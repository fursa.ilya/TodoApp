package com.todo.fursa.di

import com.todo.fursa.AppDelegate
import com.todo.fursa.data.repository.LocalRepository
import com.todo.fursa.ui.activity.MainActivity
import com.todo.fursa.ui.dialog.MainBottomSheetDialog
import com.todo.fursa.ui.viewmodel.AppViewModel
import com.todo.fursa.util.helpers.NotificationHelper
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RoomModule::class, AppModule::class])
abstract class AppComponent {
    abstract fun inject(viewModel: AppViewModel)
    abstract fun inject(app: AppDelegate)
    abstract fun inject(mainActivity: MainActivity)
    abstract fun inject(repository: LocalRepository)
    abstract fun inject(notificationHelper: NotificationHelper)
}