@file:JvmName("ToastUtils")

package com.todo.fursa.util

import android.content.Context
import android.widget.Toast

fun toast(ctx: Context, message: String) {
    Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show()
}

