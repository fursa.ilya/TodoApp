package com.todo.fursa.util

import android.graphics.Bitmap

interface FileAttachListener {
    fun onPictureAttached(bitmap: Bitmap)
}