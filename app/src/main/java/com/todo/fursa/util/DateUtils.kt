@file:JvmName("DateUtils")

package com.todo.fursa.util
import java.text.SimpleDateFormat
import java.util.*

const val DATE_TIME_PATTERN = "dd.MM.yyyy HH:mm"

val dateFormat by lazy { SimpleDateFormat(DATE_TIME_PATTERN) }
fun dateToString(date: Date?): String { return dateFormat.format(date) }
fun fromTimestamp(timestamp: Long): String? { return dateFormat.format(timestamp) }
fun timestampToDate(timestamp: Long): Date? { return Date(timestamp) }
