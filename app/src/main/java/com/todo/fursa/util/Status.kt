package com.todo.fursa.util

enum class Status(status: Int) {
    NORMAL(0),
    HIGH(1)
}