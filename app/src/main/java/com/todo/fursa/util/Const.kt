package com.todo.fursa.util

object Const {
    const val DB_NAME = "inner.db"
    const val NOTE_TABLE = "note"
    const val NOTE_ID = "id"
    const val NOTE_TITLE = "title"
    const val NOTE_CONTENT = "content"
    const val NOTE_STATUS = "status"
    const val NOTE_START_TIME = "createdTime"
    const val NOTE_UPDATED_TIME = "updatedTime"
    const val NOTE_FINISH_TIME = "finishTime"
}
