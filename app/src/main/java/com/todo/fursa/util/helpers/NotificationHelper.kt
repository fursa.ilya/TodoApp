package com.todo.fursa.util.helpers

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Color
import android.os.Build
import android.os.Build.ID
import androidx.annotation.RequiresApi
import com.todo.fursa.AppDelegate
import com.todo.fursa.R
import javax.inject.Inject


const val ID = "fursa.notif.id"
const val NAME = "fursa.notif.channel"
@RequiresApi(Build.VERSION_CODES.O)
class NotificationHelper(context: Context): ContextWrapper(context) {

    init {
        AppDelegate.appComponent.inject(this@NotificationHelper)
        val channel = NotificationChannel(ID, NAME, NotificationManager.IMPORTANCE_DEFAULT)
        channel.enableLights(true)
        channel.enableVibration(true)
        channel.lightColor = Color.GREEN
        channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 500, 200, 500)
        manager.createNotificationChannel(channel)
    }

    @Inject
    lateinit var manager: NotificationManager

    companion object {
        fun getNotification(title: String, content: String, context: Context): Notification.Builder {
            return Notification.Builder(context.applicationContext, com.todo.fursa.util.helpers.ID)
                    .setContentTitle(title)
                    .setContentText(content)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
        }
    }
}