package com.todo.fursa

import android.app.Application
import com.google.android.libraries.places.api.Places
import com.todo.fursa.di.AppComponent
import com.todo.fursa.di.AppModule
import com.todo.fursa.di.DaggerAppComponent
import com.todo.fursa.di.RoomModule


class AppDelegate: Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDaggerAppComponent()
        initPlacesAutocompleteApi()
    }

    private fun initPlacesAutocompleteApi() {
        Places.initialize(applicationContext, BuildConfig.GOOGLE_PLACES_API_KEY)
        Places.createClient(this)
    }

    private fun initDaggerAppComponent() {
        appComponent = DaggerAppComponent
                .builder()
                .roomModule(RoomModule(this@AppDelegate))
                .appModule(AppModule(this@AppDelegate))
                .build()
        appComponent.inject(this@AppDelegate)
    }

}